var gulp,util,gulpRequireTasks;

gulp 				= require('gulp');
util 				= require('gulp-util');
gulpRequireTasks 	= require('gulp-require-tasks');

gulpRequireTasks({
	path:__dirname + '/gulp/tasks'
});

gulp.task('default',function(){
	return util.log(util.colors.red('This is the default task, specify which task to run.'));
});