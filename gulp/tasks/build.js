var util,coffee,concat,rename,minify,wrap;

util 	= require('gulp-util');
coffee 	= require('gulp-coffee');
concat 	= require('gulp-concat');
rename 	= require('gulp-rename');
minify 	= require('gulp-uglify');
wrap 	= require('gulp-wrap');

module.exports = function(gulp,cb){
	gulp.src(process.cwd() + '/orchestra_modules/**/src/*.coffee')
		.pipe(coffee({bare:true}))
		.pipe(wrap('define(function(){<%= contents %>})',{},{parse:false}))
		.pipe(minify())
		.pipe(rename(function(path){
			path.dirname += '/../compiled/'; 
		}))
		.pipe(gulp.dest(function(file){return file.base;}));
}