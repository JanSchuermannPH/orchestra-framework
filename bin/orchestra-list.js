module.exports = function(args){
	var cwd,root,fs,config,path;
	cwd = process.cwd();
	fs = require('fs');

	fs.readFile(cwd + '/Orchestra.json','utf8',function(err,data){
		if(err)
		{
			console.log('Orchestra has not been initialized in this directory yet, run "orchestra init" first.\n');
		}
		else
		{
			config = JSON.parse(data);
			fs.readdir(cwd + '/' + config.root + '/modules',function(err,files){
				console.log('\n' + files.length + ' Module' + (files.length > 1 ? 's' : '') + ' installed:\n');
				files.forEach(function(cv,i,a){
					cv = cv.replace(/orchestra-/,'');
					cv = cv.charAt(0).toUpperCase() + cv.slice(1,cv.length);
					console.log('- ' + cv);
				});
			});
		}
	});
}