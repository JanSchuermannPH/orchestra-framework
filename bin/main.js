#!/usr/bin/env node

var cmd;

cmd = require('yargs');

cmd
	.usage("Usage: orchestra <cmd> [args]")
	.command('version','Show the current Version',{},function(args){console.log(require('../package.json').version);})
	.command('init','Initialize Orchestra in the current directory',{},function(args){require('./orchestra-init')(args)})
	.command('update','Update Orchestra loader',{},function(args){require('./orchestra-update')()})
	.command('list','List installed modules',{},function(args){require('./orchestra-list')()})
	.command('refetch','Update modules',{},function(args){require('./orchestra-refetch')()})
	.command('add','Add module',function(yargs){
		return yargs.option('module', {
        	alias: 'module',
        	describe: 'The module to add'
      	})
	},function(args){require('./orchestra-add-module')(args)})
	.command('remove','Remove module',function(yargs){
		return yargs.option('module', {
        	alias: 'module',
        	describe: 'The module to remove'
      	})
	},function(args){require('./orchestra-remove-module')(args)})
	//.command('create','Create module',{},function(args){require('./orchestra-create-module')(args)})
	//.command('config','Load new configuration',{},function(args){require('./orchestra-load-config')(args)}) //after loading new config, need to update project
	.help()
	.argv;