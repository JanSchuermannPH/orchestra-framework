function getModulePaths(modules)
{
	var modulePaths = "		'orchestra-core':'orchestra-core/orchestra-core',\n";
	modules.forEach(function(cv,i,a){
		if(cv !== 'orchestra-core') modulePaths += "		'"+cv+"':'"+cv+"/"+cv+"',\n";
	});
	modulePaths = modulePaths.slice(0,modulePaths.length-2) + '\n';
	return modulePaths;
}
function getModuleRequires(modules)
{
	var moduleRequires = "	'orchestra-core',\n";
	modules.forEach(function(cv,i,a){
		if(cv !== 'orchestra-core') moduleRequires += "	'"+cv+"',\n";
	});
	moduleRequires = moduleRequires.slice(0,moduleRequires.length-2) + '\n';
	return moduleRequires;
}
function getModuleRequiresArguments(modules)
{
	var moduleRequiresArguments = "Orchestra,";
	modules.forEach(function(cv,i,a){
		if(cv !== 'orchestra-core')
		{
			var moduleName = cv.replace('orchestra-','');
			moduleName = moduleName.charAt(0).toUpperCase() + moduleName.slice(1,moduleName.length);
			moduleRequiresArguments += moduleName + ',';
		}
	});
	moduleRequiresArguments = moduleRequiresArguments.slice(0,moduleRequiresArguments.length-1);
	return moduleRequiresArguments;
}
module.exports = function(args){
	var fs,config;
	fs = require('fs');
	config = require(process.cwd() + '/Orchestra.json');
	fs.readFile(process.cwd() + '/' + config.root + '/scripts/loader.js','utf8',function(err,data){
		if(err) throw err;
		var loaderScript =
			"/*********************************************************/\n" + 
			"/*                                                       */\n" +
			"/*                  RequireJS Configuration              */\n" +
			"/*         Feel free to adjust however you want to,      */\n" +
			"/*    but keep in mind that the paths need to reference  */\n" +
			"/*       the correct physical paths in your project!     */\n" +
			"/*                                                       */\n" +  
			"/*********************************************************/\n" + 
			"requirejs.config({\n"+
			"	baseUrl:'./" + config.root + "/modules',\n" +
			"	paths:{\n" + 
					getModulePaths(config.modules) + 
			"	}\n" +
			"});\n" +
			"\n\n" +
			"/*********************************************************/\n" +
			"/*                                                       */\n" +
			"/*                     RequireJS Setup                   */\n" +
			"/*                   PLEASE DO NOT TOUCH,                */\n" +
			"/*            UNLESS YOU KNOW WHAT YOU ARE DOING!        */\n" +
			"/*                                                       */\n" +  
			"/*********************************************************/\n" + 
			"requirejs([\n" +
				getModuleRequires(config.modules) +
			"],function(){\n" +
			"	window['Orchestra'] = new _Orchestra();\n" +
			"	require(['../scripts/scripts'],function(){})" + 
			"})\n";
		fs.writeFile(process.cwd() + '/' + config.root + '/scripts/loader.js',loaderScript,function(err){
			if(err) throw err;
		});
	});
}