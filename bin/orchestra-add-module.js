module.exports = function(args){
	if(typeof args._[1] !== 'undefined')
	{
		var module,config,targetfile,ncp;

		ncp = require('ncp').ncp;
		ncp.limit = 16;
		module = args._[1];
		targetfile = process.cwd() + '/Orchestra.json';
		config = require(targetfile);
		if(config.modules.indexOf(module) < 0)
		{
			console.log('Adding module "' + module + '"...\n');
			ncp(__dirname + '/../orchestra_modules/' + module,process.cwd() + '/' + config.root + '/modules/' + module,function(err){
				if(err) 
				{
					console.log(err);
					console.log('Module not found');
					return null;
				}
				config.modules.push(module);
				require('jsonfile').writeFileSync(targetfile,config,{spaces:2});
				console.log('Updating Orchestra loader...');
				require(__dirname + '/orchestra-update')();
				console.log('Done');
			});
		}
		else
		{
			console.log('Module already exists!');
			return null;
		}
		
	}
	else
	{
		console.log('Don\'t know which module to add');
	}
}