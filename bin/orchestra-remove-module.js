module.exports = function(args){
	if(typeof args._[1] !== 'undefined')
	{
		var module,config,targetfile,ncp;

		ncp = require('ncp').ncp;
		ncp.limit = 16;
		module = args._[1];
		targetfile = process.cwd() + '/Orchestra.json';
		config = require(targetfile);
		if(config.modules.indexOf(module) > 0)
		{
			console.log('Removing module "' + module + '"...\n');
			config.modules.splice(config.modules.indexOf(module),1);
			require('jsonfile').writeFileSync(targetfile,config,{spaces:2});
			//Remove folder
			require('rimraf')(process.cwd() + '/' + config.root + '/modules/' + module,function(err){
				if(err) throw err;
			});
			console.log('Updating Orchestra loader...');
			require(__dirname + '/orchestra-update')();
			console.log('Done');
		}
		else
		{
			console.log('Module does not exist!');
			return null;
		}
		
	}
	else
	{
		console.log('Don\'t know which module to add');
	}
	
}