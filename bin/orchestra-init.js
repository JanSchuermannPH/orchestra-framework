module.exports = function(args){
	var config,targetfile,fs,paths,ncp,modules_src,target_root,src;
	paths = [
		'modules',
		'src',
		'src/scripts',
		'src/stylesheets',
		'stylesheets',
		'scripts'
	];
	fs = require('fs');
	ncp = require('ncp').ncp;
	ncp.limit = 16;
	targetfile = process.cwd() + '/Orchestra.json';
	src = __dirname + '/../';
	modules_src = src + 'orchestra_modules' + '/';
	config = require(__dirname + '/../config/Orchestra.json');
	config.name = (typeof args._[1] !== 'undefined') ? args._[1] : config.name;
	fs.readdir(modules_src,function(err,files){
		if(err) throw err;
		files.forEach(function(cv,i,a){
			if(config.modules.indexOf(cv) < 0) config.modules.push(cv);
		});
		fs.readFile(targetfile,function(err){
			if(err)
			{
				console.log('\nInitializing Orchestra...\n\n');
				console.log('\nCreating configuration file...');
				require('jsonfile').writeFileSync(targetfile,config,{spaces:2});
				target_root = process.cwd() + '/' + config.root + '/';
				console.log('Done');
				console.log('\nCreating folder structure...');
				fs.readdir(process.cwd() + '/' + config.root,function(err){
					if(err) fs.mkdirSync(target_root);
					paths.forEach(function(cv){
						fs.mkdirSync(target_root + cv,644,function(err){if(err)throw err;});
					});
					console.log('Done');
					console.log('\nCopying modules...');
					config.modules.forEach(function(cv,i,a){
						var destination = target_root + '/modules/';
						ncp(modules_src + cv + '/compiled',destination + cv,function(err){
							if(err) throw err;
						});
					});
					console.log('Done');
					console.log('\nCopying Stylesheets...');
					ncp(src + 'stylesheets',target_root + '/stylesheets',function(err){
						if(err) throw err;
						console.log('Done');
						console.log('\nCopying Scripts...');
						ncp(src + 'scripts',target_root + '/scripts',function(err){
							if(err) throw err;
							console.log('Done');
							console.log('\nCopying Source...');
							ncp(src + 'bin/scripts',target_root + '/src/scripts',function(err){
								if(err) throw err;
								ncp(src + 'bin/stylesheets',target_root + '/src/stylesheets',function(err){
									if(err) throw err;
									console.log('Done');
									console.log('Updating Orchestra loader...');
									require(__dirname + '/orchestra-update')();
									console.log('Done');
								});
							});
						});
					});
				});
			}
			else
			{
				console.log('\nError: Orchestra has already been initialized in this directory!\n');
			}
		});
	});	
}