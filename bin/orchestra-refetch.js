module.exports = function(args){
	var ncp,config,src,modules_src,target_root;
	ncp = require('ncp').ncp;
	ncp.limit = 16;
	config = require(process.cwd() + '/Orchestra.json');
	src = __dirname + '/../';
	modules_src = src + 'orchestra_modules' + '/';
	target_root = process.cwd() + '/' + config.root + '/';
	config.modules.forEach(function(cv,i,a){
		var destination = target_root + '/modules/';
		ncp(modules_src + cv + '/compiled',destination + cv,function(err){
			if(err) throw err;
			console.log("Refetched Module '" + cv + "'");
			if(i === config.modules.length-1){
				console.log("\nRefetched " + config.modules.length + " Module" + (config.modules.length > 1 ? 's':''));
			}
		});
	});
}